package IntToAbstractToClasses;

public class RunApplication {
    public static void main(String[] args) {
        Cat kitty = new Cat("MYAAAAAAAAAW!");
        Dog puppy = new Dog("Bark!Bark!");

        puppy.saySomething();
        kitty.saySomething();

    }
}
