package AutomationTesting;

import PetsSounds.Cat;
import PetsSounds.Dog;
import PetsSounds.MakeNoize;
import org.testng.Assert;
import org.testng.annotations.Test;


public class WhatSoundPetsMakesTestCase {
    @Test

    public void whatSound(){
        Cat murchik = new Cat("test");
        Assert.assertEquals(murchik.actionMyav, "test");

        Dog barsik = new Dog("test2");
        Assert.assertEquals(barsik.actionGav, "test2");

    }
}
